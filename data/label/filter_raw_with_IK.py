# projcess raw file so only IK people remains

## 3D root, id=14
## 2D neck, id=1

def pose_error():

    ## 3D
    # NECK_ID = 1
    # ROOT_ID = 14
    # SPINE_ID = 15
    # HEAD_ID = 16
    
    arr = [1, 14, 15, 16]

    error = 0
    for i in arr:
        joint_pos = 3 * (i + 1) - 1
        error += sum([ pow((float(IK_line[pos]) - float(raw_line[pos])), 2) for pos in [joint_pos, joint_pos+1, joint_pos+2] ])
    
    return error

IKFile= open("my.txt") 
outputFile3D = open("post_raw3D.txt", "w")
outputFile2D = open("post_raw2D.txt", "w")

### for each IK line ###
while True:

    # get new IK line
    IK_line = IKFile.readline().split()
    if len(IK_line) == 0:
        break

    # init
    min_error = 9999999999999999
    match_raw_line_id = -1
    raw_line_id = -1

    rawFile = open("raw3D.txt") 
    ### search the matching raw line ###
    for i, line in enumerate(rawFile):
        raw_line = line.split()

        if len(raw_line) != 0 and int(raw_line[0]) == int(IK_line[0]):
            # compare
            error = pose_error()
            if error < min_error:
                min_error = error
                match_raw_line_id = i

        elif(len(raw_line) == 0 or int(raw_line[0]) > int(IK_line[0])):
            break
    rawFile.close()

    ##  write the match line 3D
    rawFile = open("raw3D.txt") 
    for i, line in enumerate(rawFile):

        # change ID to IK's ID
        line = line.split()
        line[1] = IK_line[1]
        line = " ".join(line) + "\n"

        # write line
        if i == match_raw_line_id:
            outputFile3D.write(line)
            break
    rawFile.close()

    ##  write the match line 2D
    rawFile = open("raw2D.txt") 
    for i, line in enumerate(rawFile):

        # change ID to IK's ID
        line = line.split()
        line[1] = IK_line[1]
        line = " ".join(line) + "\n"
        
        # write line
        if i == match_raw_line_id:
            outputFile2D.write(line)
            break
    rawFile.close()
    
outputFile3D.close()
outputFile2D.close()
IKFile.close()



