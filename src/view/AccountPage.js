import { withStyles } from "@material-ui/core/styles"
import Container from '@material-ui/core/Container';
import Box from '@material-ui/core/Box';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';

import Checkbox from '@material-ui/core/Checkbox';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';
import DeleteIcon from '@material-ui/icons/Delete';

import Tab from '@material-ui/core/Tab';
import Tabs from '@material-ui/core/Tabs';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';

import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';

import Snackbar from '@material-ui/core/Snackbar';
import Alert from '@material-ui/lab/Alert';

const useStyles = theme => ({
  title:{
    padding: theme.spacing(8, 0, 0),
  },
  button:{
    margin: theme.spacing(2, 1),
  },
});

const TableRowBody = (props) => {
  const { info, keyId, onDelete } = props;

  const handleChange = (event, name) => {
    let value;
    if(name==='account')
      value = event.target.value;
    else
      value = event.target.checked;
    props.onChange(props.keyId, name, value);
  };

  return(
    <TableRow key={keyId}>
      <TableCell component="th" scope="row">
        {info.name}
        <IconButton aria-label="delete" onClick={()=>{onDelete(keyId)}}>
          <DeleteIcon fontSize="small"/>
        </IconButton>
      </TableCell>
      
      <TableCell align="right">
          <Select
            value={info.account}
            onChange={(event)=>{handleChange(event, 'account')}}
            displayEmpty
            inputProps={{ 'aria-label': 'Without label' }}
          >
            <MenuItem value='admin'>管理員</MenuItem>
            <MenuItem value='normal'>一般</MenuItem>
            <MenuItem value='null'>無</MenuItem>
          </Select>
      </TableCell>

      <TableCell align="right">
        <Checkbox checked={info.customize} size='small' color="primary"
          onChange={(event)=>{handleChange(event, 'customize')}}
        />
      </TableCell>

      <TableCell align="right">
        <Checkbox checked={info.video} size='small' color="primary"
          onChange={(event)=>{handleChange(event, 'video')}}
        />
      </TableCell>

      <TableCell align="right">
        <Checkbox checked={info.labeling} size='small' color="primary"
          onChange={(event)=>{handleChange(event, 'labeling')}}  
        />
      </TableCell>

      <TableCell align="right">
        <Checkbox checked={info.view} size='small' color="primary"
          onChange={(event)=>{handleChange(event, 'view')}}
        />
      </TableCell>

    </TableRow>
  );
}

class AccountPage extends React.Component {

  createData(name, account, customize, video, labeling, view) {
    return { name, account, customize, video, labeling, view};
  }

  constructor(props) {
    super(props);
    this.state = {
      openDialog: false,
      accountIndex: 0,
      accountGroup: ['桌球', '拳擊', '棒球'],
      accountList: [
        this.createData('tabletennis_coach', 'normal', true, true, true, true),
        this.createData('tabletennis_student', 'normal', false, false, true, true),
        this.createData('tabletennis_viewer', 'normal', false, false, false, true),
      ],
      addAccount:{
        account: '',
        password: ''
      },
      snackbar:{
        open: false
      }
    }
  }

  // group
  switchGroup = (e, value) => {
    this.setState({accountIndex: value});
    // TODO Update accountlist state new accout list
  }

  // table
  a11yProps(index) {
    return {
      id: `scrollable-auto-tab-${index}`,
      'aria-controls': `scrollable-auto-tabpanel-${index}`,
      key: index
    };
  }
  updateRow = (index, name, value) => {
    this.setState((state) => {
      // lazy zzz...
      state.accountList[index][name] = value;
      return {
        accountList: state.accountList
      }
    });
  }

  addNewAccount = (e) => {
    this.setState((state) => {
      const newAccountList = state.accountList.concat([
        this.createData(state.addAccount.account, 'null', false, false, false, false)
      ]);
      return {
        accountList: newAccountList
      }
    });
    this.openSnackbar();
  }

  clickAddAccount = () => {
    this.setState({
      openDialog: true,
      addAccount:{
        account: '',
        password: ''
      },
    })
  }
  deleteAccount = (ind) => {
    const accountList = this.state.accountList;
    let filtered = [];
    for(let i=0; i<accountList.length; i++){
      if(i!== ind){
        filtered.push(accountList[i])
      }
    }
    this.setState({
      accountList: filtered
    });
  }

  closeDialog = () => {
    this.setState({openDialog: false})
  }

  openSnackbar = () => {
    this.setState({snackbar: {
      open: true
    }})
  }

  closeSnackbar = () => {
    this.setState({snackbar: {
      open: false
    }})
  }

  changeTextField = (e) => {
    let addAccount = Object.assign({}, this.state.addAccount);
    addAccount[e.target.id] = e.target.value;

    this.setState({
      addAccount: addAccount
    });
  }

  render() {
    const { classes, match } = this.props;
    const { accountIndex } = this.state;
    return (
      <Container maxWidth="md">
        <Snackbar 
          anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
          open={this.state.snackbar.open} autoHideDuration={3000} 
          onClose={this.closeSnackbar}
        >
          <Alert onClose={this.closeSnackbar} severity="success">
            新增成功
          </Alert>
        </Snackbar>

        <div className={classes.title}>
          <Typography component="h4" variant="h4" color="textPrimary" gutterBottom>
            Account Manager
          </Typography>
        </div>
        <Tabs
          value={this.state.accountIndex}
          onChange={this.switchGroup}
          indicatorColor="primary"
          textColor="primary"
          variant="scrollable"
          scrollButtons="auto"
          aria-label="scrollable auto tabs example"
        >
          {this.state.accountGroup.map((value, index) => {
            return(
              <Tab label={value} {...this.a11yProps(index)} />
            )
          })}
        </Tabs>
        <TableContainer component={Paper}>
          <Table className={classes.table} size="small" aria-label="a dense table">
            <TableHead>
              <TableRow>
                <TableCell>名稱</TableCell>
                <TableCell align="right">管理帳號</TableCell>
                <TableCell align="right">客製化</TableCell>
                <TableCell align="right">上傳影片</TableCell>
                <TableCell align="right">標注</TableCell>
                <TableCell align="right">檢視</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              { this.state.accountList.map((info, index)=>{
                return(
                  <TableRowBody 
                    key={index} 
                    info={info} 
                    onChange={this.updateRow}
                    onDelete={this.deleteAccount}
                    keyId={index}
                  />
                );
              })}
            </TableBody>
          </Table>
        </TableContainer>
        <Box display="flex" justifyContent="flex-end">
          <Button className={classes.button} 
            color="primary" variant="contained" 
            onClick={this.clickAddAccount}
          >
            新增帳號
          </Button>
        </Box>
        <Dialog open={this.state.openDialog} 
          onClose={this.closeDialog} 
          aria-labelledby="form-dialog-title"
        >
          <DialogTitle id="form-dialog-title">新增帳號</DialogTitle>
          <DialogContent>
            <TextField
              value={this.state.addAccount.account}
              onChange={(e)=>{this.changeTextField(e)}}
              autoFocus
              margin="dense"
              id="account"
              label="帳號"
              type="email"
              fullWidth
            />
            <TextField
              value={this.state.addAccount.password}
              onChange={(e)=>{this.changeTextField(e)}}
              margin="dense"
              id="password"
              label="密碼"
              type="password"
              fullWidth
            />
          </DialogContent>
          <DialogActions>
            <Button onClick={this.closeDialog} color="primary">
              取消
            </Button>
            <Button onClick={(e)=>{this.closeDialog(e); this.addNewAccount(e);}} color="primary">
              新增
            </Button>
          </DialogActions>
        </Dialog>
      </Container>   
    );
  }
}

export default withStyles(useStyles)(AccountPage);