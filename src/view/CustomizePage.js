const useStyles = theme => ({
  title:{
    padding: theme.spacing(8, 0, 6),
  }
});

class CustomizePage extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {
    const { classes } = this.props;

    return (
      <Container maxWidth="md">
        <div className={classes.title}>
          <Typography component="h4" variant="h4" color="textPrimary" gutterBottom>
            Demo
          </Typography>
        </div>
      </Container>
    );
  }
}

export default withStyles(useStyles)(CustomizePage);