import {
  Redirect,
  Link,
  Switch,
  Route,
  useRouteMatch
} from "react-router-dom";

import { withStyles } from '@material-ui/core';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';

import NavBar from "../component/NavBar";
import MatchController from "../component/MatchController";

const useStyles = theme => ({
  title:{
    padding: theme.spacing(8, 0, 0),
  }
});

class MainPage extends React.Component {

  constructor(props) {
    super(props);
  }

  // TODO: Nested routing
  render() {
    const { classes, match } = this.props;

    return (
      <div>
        {/* {!this.props.auth.isLogin()
          && <Redirect to="/login"/>
        } */}
        <NavBar/>
        <Switch>
          <Route path={match.url}>
            <Container maxWidth="lg">
              {/* <div className={classes.title}>
                <Typography component="h4" variant="h4" color="textPrimary" gutterBottom>
                  {match.path}
                </Typography>
              </div>
              <Link to="/labeling">Labeling Page</Link> */}
              <MatchController/>
            </Container>
          </Route>
        </Switch>
      </div>
    );
  }
}

export default withStyles(useStyles)(MainPage);