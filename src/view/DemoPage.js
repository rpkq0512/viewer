import { withStyles } from '@material-ui/core';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';

import SingleSelecter from "../component/base/SingleSelecter";
import VideoController from "../component/VideoController";

const useStyles = theme => ({
  title:{
    padding: theme.spacing(8, 0, 0),
  }
});

class DemoPage extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      selectOption0:{
        name:"拍面",
        options:[
          {
            tags:["正拍", "反拍"]
          },        
        ]
      },
      selectOption1:{
        name:"動作",
        options:[
          {
            title:"控制",
            tags:["擺", "切", "劈"]
          },
          {
            title:"防守",
            tags:["擋", "削", "放高球"]
          }
        ]
      }
    }
  }

  render() {
    const { classes } = this.props;

    return (
      <Container maxWidth="md">
        <div className={classes.title}>
          <Typography component="h4" variant="h4" color="textPrimary" gutterBottom>
            Demo
          </Typography>
          <SingleSelecter options={this.state.selectOption1}/>
          {/* <SingleSelecter edit/> */}
        </div>
        <VideoController/>
      </Container>
    );
  }
}

export default withStyles(useStyles)(DemoPage);