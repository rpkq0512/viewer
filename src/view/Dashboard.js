import { withStyles } from '@material-ui/core';
import {
  Redirect,
  Link
} from "react-router-dom";

import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';

const useStyles = theme => ({
  title:{
    padding: theme.spacing(8, 0, 0),
  }
});

class Dashboard extends React.Component {

  constructor(props) {
    super(props);
    console.log("constructor");
  }

  render() {
    const { classes } = this.props;
    return (
      <div>
        {!this.props.auth.isLogin()
          && <Redirect to="/login"/>
        }
        <Container maxWidth="md">
          <div className={classes.title}>
            <Typography component="h4" variant="h4" color="textPrimary" gutterBottom>
              Dashboard
            </Typography>
          </div>
          <Link to="/tabletennis">Tabletennis</Link>   
        </Container>
      </div>
    );
  }
}

export default withStyles(useStyles)(Dashboard);