import {
    BrowserRouter as Router,
    Switch,
    Route
  } from "react-router-dom";
  import CssBaseline from '@material-ui/core/CssBaseline';
  
  import Global from "./data/Global"
  import Authentication from "./Authentication"
  
  import Dashboard from "./view/Dashboard"
  import MainPage from "./view/MainPage"
  import LoginPage from "./view/LoginPage";
  import DemoPage from "./view/DemoPage";
  import LabelingPage from "./view/LabelingPage";
  import AccountPage from "./view/AccountPage";
  
  const auth = new Authentication();
  
  class Root extends React.Component {
  
    constructor(props) {
      super(props);
      console.log("constructor");
      console.log(Global.Instance.name)
    }
  
    componentDidMount() {
      document.title = 'Court Athena';
    }
  
    render() {
      return (
        <React.Fragment>
          <CssBaseline />
          <Router>
            <Switch>
              <Route path="/login">
                <LoginPage auth={auth}/>
              </Route>
              <Route path="/demo">
                <DemoPage/>
              </Route>
              <Route path='/tabletennis' component={(props) => 
                <MainPage auth={auth} match={props.match}/>} 
              />
              <Route path='/labeling' component={(props) => 
                <LabelingPage auth={auth} match={props.match}/>} 
              />
              <Route path='/account' component={(props) => 
                <AccountPage auth={auth} match={props.match}/>} 
              />
              <Route path="/">
                <Dashboard auth={auth}/>
              </Route>
            </Switch>
          </Router>
        </React.Fragment>
      );
    }
  }
  
  ReactDOM.render(<Root />, document.getElementById("root"));
  