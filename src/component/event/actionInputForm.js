import * as React from "react";
import { SingleSelect } from "react-select-material-ui";
import Chip from '@material-ui/core/Chip';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { withStyles, makeStyles } from '@material-ui/core';


const useStyles = (theme) => ({
  multiButton:{
    paddingBottom: theme.spacing(2),
  },
  labelName:{
    marginRight: theme.spacing(2),
    marginLeft: theme.spacing(2),
    display: "inline-block"
  },
  selectButton:{
    marginRight: theme.spacing(1),
    marginTop: theme.spacing(2),
  }
});

export default class ActionInputForm extends React.Component {

  constructor(props) {
    super(props);

  }
  render() {
    return (
      <div>
        <Grid container>
           <Grid item xs={4}>
             <Chip label="板數: 10" />
           </Grid>
           <Grid item xs={4}>
             <Chip label="擊球球員: 小明" />
           </Grid>
           <Grid item xs={4}>
             <Chip label="時間: 10:10" />
           </Grid>
         </Grid>
         <hr/>
        {this.props.eventInfos.buttonInfos.eventlabels.map((eventlabel, index) => {
          return <Event key={index} 
                        eventlabel={eventlabel} 
                        handleEditor={this.props.handleEditor}
                        eventInfos={this.props.eventInfos}/>;
        })}
      </div>
    );
  }
}

class Event extends React.Component{
  constructor(props){
    super(props);
  }
  render(){
    return(
      <div>
        <Typography 
                    variant="body1" color="textPrimary" gutterBottom>
          {this.props.eventlabel.name}
        </Typography>
        {
          this.props.eventlabel.labels.map((label,index)=>{
            return <LabelAction key={index} 
                                labelActions={label} 
                                handleEditor={this.props.handleEditor} 
                                eventInfos={this.props.eventInfos}/>
          })}
      </div>
    );
  }
}

class _LabelAction extends React.Component{
  constructor(props){
    super(props);
  }
  render(){
    const { classes } = this.props;
    return(
      <div className={classes.multiButton}>
        <Typography 
          variant="body1" 
          color="textPrimary" 
          gutterBottom
          className={classes.labelName}>
            {this.props.labelActions.actionType}
        </Typography>
        {
          this.props.labelActions.actions.map((action,index)=>{
          return <Button 
                    key={index} 
                    value={action} 
                    onClick={this.props.handleEditor}
                    variant="contained"
                    size="small"
                    color={
                      this.props.eventInfos.face===action?"primary":
                      this.props.eventInfos.action===action?"primary":
                      this.props.eventInfos.placement===action?"primary":"default"
                    }
                    className={classes.selectButton}>
                      {action}
                  </Button>
          })}
      </div>
    );
  }
}
const LabelAction = withStyles(useStyles)(_LabelAction);
