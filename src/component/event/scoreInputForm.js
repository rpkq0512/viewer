import * as React from "react";
import { SingleSelect } from "react-select-material-ui";
import Chip from '@material-ui/core/Chip';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
 
class ScoreInputForm extends React.Component {
  render() {
    const faceOptions = ["Africa", "America", "Asia", "Europe"];
 
    return (
      <div>
        <Chip label="時間: 10:10" />
        <Grid container
        alignItems="center"
        justify="center"
        spacing={1}
        >
          <Grid item xs={6}>
            <TextField  id='p1Score' label="先發球員" onChange={this.props.handleForm}/>
          </Grid>
          <Grid item xs={6}>
            <TextField  id='p2Score' label="後發球員" onChange={this.props.handleForm}/>
          </Grid>
        </Grid>
      </div>
    );
  }
 

}

export default ScoreInputForm;