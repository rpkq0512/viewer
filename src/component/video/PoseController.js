
import * as THREE from "three";
import { DragControls } from "./DragControls"
import  {TransformControls} from "./TransformControls"
import PoseLoader from "./PoseLoader";
import SelectionController from "./SelectionController";

const humanColors = [
  0xcccccc,
  0xcccc33,
  0xcc33cc,
  0x33cccc,
  0xcc3333,
  0x33cc33,
  0x3333cc,

  0xcc9999,
  0xcc9933,
  0xcc3399,
  0x339999,
  0x993333,
  0x339933,
  0x333399,
];

  // --- poseMap structure ---
  // poseMap.get(frameID):
  //     humanID: {
  //       keypoints: [...],
  //       limcs: [keypoints pair ...]
  //     }
  //   }
  // 

  // --- Obj structure ---
  // - poseObj[humanID]: a poseGroup
  // - a poseGroup: a lot of kp and limbs
  //  
export default class PoseController {

  constructor(screenDimension, videoDimension, scene, layer) {

    this.screenDimension = screenDimension
    this.videoDimension = videoDimension
    this.frameID = -1

    this.layer = layer
    this.scene = scene

    // ------------
    // pooling
    // -------------

    const [kp_pool, draggables] = this.createKpPool(100);
    const limb_pool = this.createLimbPool(200);
    
    // -------------
    // selection control
    // -------------
    const poseLoader = new PoseLoader()
    
    // ------------
    // set to "this"
    // ------------ 
    this.dragControls = []
    this.selectionControl = null
    
    this.kp_pool = kp_pool;
    this.limb_pool = limb_pool;

    this.draggables = draggables
    
    this.poseLoader = poseLoader
  }

  // ---------------------
  // ----- API -----------
  // ---------------------

  loadPose(path, is2D_3D, isIK_raw, poseType)
  {
    if(this.poseMap)
    {
      this.poseMap.clear()
      delete this.poseMap
    }

    switch(poseType)
    {
      case "xnect":
        this.poseMap = this.poseLoader.loadXNectPose(path, is2D_3D, isIK_raw);
        break;
    }

    this.is2D_3D = is2D_3D
  }

  createDragControl(camera, renderer, cameraController=null)
  {
    const jointControl = new DragControls(this.draggables, camera, renderer.domElement, this.layer);
    this.jointControl = jointControl
    this.jointControl.mouse = {
      x: 0,
      y: 0,
      z: 0
    }

    jointControl.addEventListener( 'dragstart', ( event ) => 
    {
      const x = event.object.position.x
      const y = event.object.position.y
      const z = event.object.position.z
      this.jointControl.mouse = {
        x: x,
        y: y,
        z: z
      }

      // --- stop camera --- //
      if(cameraController)
        cameraController.enabled = false;

      // --- get the limbs to be updated --- //
      this.jointControl.limbToUpdate = []
      this.jointControl.limbToUpdate = this.getConnectLimbs(event.object)
      // console.log(this.dragControls.limbToUpdate)

    });

    jointControl.addEventListener( 'drag', ( event ) => 
    {
      const x = event.object.position.x
      const y = event.object.position.y
      const z = event.object.position.z
      this.jointControl.mouse = {
        x: x,
        y: y,
        z: z
      }

      this.updateJointPosition(event.object, this.jointControl)
    });

  }

  createTransformControl(camera, renderer, scene, box, cameraController=null)
  {
    const jointControl = new TransformControls( camera, renderer.domElement, this.layer, this.draggables);
    this.jointControl = jointControl
    this.jointControl.mouse = {
      x: 0,
      y: 0,
      z: 0
    }

    box.layers.set(this.layer);
    jointControl.attach(box)
    scene.add(jointControl)
    
    var self = this
    
    jointControl.addEventListener( 'dragging-changed', function ( event ) {
      cameraController.enabled = ! event.value;
    } );

    jointControl.addEventListener( 'selectJoint', function ( event ) {

      const x = event.object.position.x
      const y = event.object.position.y
      const z = event.object.position.z
      jointControl.mouse = {
        x: x,
        y: y,
        z: z
      }

      jointControl.limbToUpdate = []
      jointControl.limbToUpdate = self.getConnectLimbs(event.object)
    } );

    jointControl.addEventListener( 'drag', function ( event ) {

      const x = event.object.position.x
      const y = event.object.position.y
      const z = event.object.position.z
      jointControl.mouse = {
        x: x,
        y: y,
        z: z
      }

      self.updateJointPosition(event.object, jointControl)
    } );

  }

  createSelectionControl(renderer)
  {
    const selectionControl = new SelectionController(humanColors)
    this.selectionControl = selectionControl

    renderer.domElement.addEventListener( 'keydown', ( event ) => 
    {
      if(event.keyCode == 90)
      {
        this.selectionControl.SelectNext(this.poseMap.get(this.frameID), this.poseObj)
      }
    });
  }

  getConnectLimbs(joint)
  {
    var connectLimbs = []

    let joint_name = joint.name.split("_");
    let joint_humanID = joint_name[2];
    let jointID = joint_name[3];
    
    this.poseObj[joint_humanID].traverse((limbObj)=>{
      let name = limbObj.name.split("_");
      if(name[0] == "limb")
      {
        let limb_humanID = name[2];
        let start = name[3];
        let end = name[4];
        if(limb_humanID == joint_humanID && (start == jointID || end == jointID))
        {
          connectLimbs.push(limbObj)
        }
      }
    })

    return connectLimbs
  }

  updateJointPosition(joint, controller)
  {
    const poseMap = this.poseMap.get(this.frameID)

      // update kp
      const name = joint.name.split("_");
      const humanID = name[2];
      const jointID = name[3];
      poseMap[humanID].keypoints[jointID] = {
        cor_x: joint.position.x,
        cor_y: joint.position.y,
        cor_z: joint.position.z,
      }
    
      if(this.is2D_3D == "3D")
      {
        // --- update kp in poseMap --- //
        poseMap[humanID].keypoints[jointID] = {
          cor_x: joint.position.x,
          cor_y: joint.position.y,
          cor_z: joint.position.z,
        }

        controller.limbToUpdate.forEach((limbObj)=>{
          let name = limbObj.name.split("_");
          let humanID = name[2];
          let start = name[3];
          let end = name[4];
          
          const keypoints = poseMap[humanID].keypoints;
          const points = [
            new THREE.Vector3(keypoints[start].cor_x, keypoints[start].cor_y, keypoints[start].cor_z),
            new THREE.Vector3(keypoints[end].cor_x, keypoints[end].cor_y, keypoints[end].cor_z)      
          ];
          limbObj.geometry.setFromPoints(points)
          limbObj.geometry.attributes.position.needsUpdate = true;
          limbObj.geometry.computeBoundingSphere();
        
        })

      } 
      else if (this.is2D_3D == "2D")
      {
        // --- update kp in poseMap --- //
        const original_x = (event.object.position.x + (this.screenDimension.width / 2)) 
        / this.screenDimension.width * this.videoDimension.width
        const original_y = (-event.object.position.y + (this.screenDimension.height / 2)) 
        / this.screenDimension.height * this.videoDimension.height
        poseMap[humanID].keypoints[jointID] = {
          cor_x: original_x,
          cor_y: original_y,
        }

        // --- redraw the limbs --- //
        controller.limbToUpdate.forEach((limbObj)=>{
          let start = name[3];
          let end = name[4];
          const keypoints = poseMap[humanID].keypoints;
          const [startOffsetX, startOffsetY] = this.rescalePos(keypoints[start].cor_x, keypoints[start].cor_y)
          const [endOffsetX, endOffsetY]  = this.rescalePos(keypoints[end].cor_x, keypoints[end].cor_y)
          const points = [
            new THREE.Vector3(startOffsetX, startOffsetY, this.layer),
            new THREE.Vector3(endOffsetX, endOffsetY, this.layer)      
          ];
          limbObj.geometry.setFromPoints(points)
          limbObj.geometry.attributes.position.needsUpdate = true;
          limbObj.geometry.computeBoundingSphere();
        })
      }  
  }

  updatePose(frame) {
    this.frameID = frame;
    const poseMap = this.poseMap.get(frame)

    // --- delete old pose --- //
    if (this.poseObj) 
    {
      for(let key in this.poseObj)
      {
        const element = this.poseObj[key]
        this.deletePose(element, this.scene);
      }
    }

    // --- create pose object --- //
    if (poseMap) 
    {
      this.poseObj = {};
      for(let humanID in poseMap)
      {
        const element = poseMap[humanID]
        this.poseObj[humanID] = this.createPoseObject(element, humanID);
        this.scene.add(this.poseObj[humanID]);
      }
    } 
  }

  // ---------------------
  // Selection Control API
  // ---------------------

  removeSelectedID(removeFromNowOn)
  {
    if(!this.selectionControl)
    {
      console.error("no selection control")
      return
    }

    const pose = this.poseObj[this.selectionControl.IDselected]
    this.deletePose(pose)
    if(removeFromNowOn == "current")
      this.selectionControl.RemoveSelectedID(this.poseMap, this.frameID)
    else if(removeFromNowOn == "after")
      this.selectionControl.RemoveSelectedIDFromNowOn(this.poseMap, this.frameID)
    else if(removeFromNowOn == "all")
      this.selectionControl.RemoveSelectedIDAllFrame(this.poseMap)
    
    console.log(this.frameID)
    console.log(this.poseMap)
  }

  setNewID(newID)
  {
    if(!this.selectionControl)
    {
      console.error("no selection control")
      return
    }

    this.selectionControl.SetNewID(newID, this.poseMap, this.frameID, this.poseObj)
  }


  // ----------------------
  // Create Pooling
  // ---------------------

  createKpPool(numKP)
  {
    const pool = [];
    const draggables = []
    
    for(let i=0; i<numKP; i++)
    {
      const geometry = (this.is2D_3D == "2D")? new THREE.CircleBufferGeometry(4) : new THREE.SphereBufferGeometry(30, 10, 10);;
      const material = new THREE.MeshBasicMaterial({
        color: 0xffffff,
      });
      const kp = new THREE.Mesh(geometry, material);
      pool.push(kp);
      draggables.push(kp);
    }

    return [pool, draggables];
  }

  createLimbPool(numLimb)
  {
    const pool = []

    for(let i=0; i<numLimb; i++)
    {
      const geometry = new THREE.BufferGeometry();
      const material = new THREE.LineBasicMaterial({
        color: 0xffffff,
      });
      const limb = new THREE.Line(geometry, material);
      pool.push(limb);
    }

    return pool;
  }

  
  // ------------------
  // create pose
  // ------------------

  createPoseObject(pose, humanID) {
    const keypoints = pose.keypoints;
    const limbs = pose.limbs;
    const poseGroup = new THREE.Group();

    if(keypoints.length == 0) 
      return poseGroup;

    // ---- keypoints ----- //
    keypoints.forEach((element, jointID) => {

      const kp = this.kp_pool.pop();
      kp.material.color.setHex(humanColors[humanID])

      if(this.is2D_3D == "2D")
      {
        kp.name = `kp_2D_${humanID}_${jointID}`
        const [offsetX, offsetY] = this.rescalePos(element.cor_x, element.cor_y);
        kp.position.set(offsetX, offsetY, this.layer);
      }
      else
      {
        kp.name = `kp_3D_${humanID}_${jointID}`
        kp.position.set(element.cor_x, element.cor_y, element.cor_z);
      }
      kp.geometry.attributes.position.needsUpdate = true;
      kp.geometry.computeBoundingSphere();

      kp.layers.set(this.layer);
      poseGroup.add(kp);
    });

    // ------ limbs ------ //
    limbs.forEach((element) => {
      const start = element.start;
      const end = element.end;
      let points = [] 

      const limb = this.limb_pool.pop();
      limb.material.color.setHex(humanColors[humanID])
      
      if(this.is2D_3D == "2D")
      {
        limb.name = `limb_2D_${humanID}_${start}_${end}`
        points = [
          new THREE.Vector3(startOffsetX, startOffsetY, this.layer),
          new THREE.Vector3(endOffsetX, endOffsetY, this.layere),
        ];
      }
      else
      {
        limb.name = `limb_3D_${humanID}_${start}_${end}`
        points = [
          new THREE.Vector3(keypoints[start].cor_x, keypoints[start].cor_y, keypoints[start].cor_z),
          new THREE.Vector3(keypoints[end].cor_x, keypoints[end].cor_y, keypoints[end].cor_z),      
        ];
      }

      // the last 2 lines prevent flickering
      limb.geometry.setFromPoints(points);
      limb.geometry.attributes.position.needsUpdate = true;
      limb.geometry.computeBoundingSphere();

      limb.layers.set(this.layer);

      poseGroup.add(limb);
    });

    poseGroup.layers.set(this.layer)

    return poseGroup;
  }

  // ---------------------
  // ------ Utility ------ 
  // ---------------------


  deleteObject(obj) {
    if (obj) {
      this.scene.remove(obj);
      obj.geometry.dispose();
      obj.material.dispose();
      obj = undefined;
    }
  }

  deletePose(group) {
    if (group) {
      group.children.forEach((element) => {
        const name = element.name.split('_');
        
        // --- pooling
        if(name[0] == "kp")
        {
          if(name[1] == "2D")
            this.kp_pool.push(element);
          else if(name[1] == "3D")
            this.kp_pool.push(element);

          this.scene.remove(element);
        }
        else if(name[0] == "limb")
        {
          this.limb_pool.push(element);
          this.scene.remove(element);
        }
        // --- delete
        else
        {
          this.deleteObject(element);
        }

      });
      this.scene.remove(group);
      group = undefined;
    }
  }

  rescalePos(original_x, original_y)
  {
    const x = (original_x / this.videoDimension.width) * this.screenDimension.width - this.screenDimension.width / 2;
    const y = -((original_y / this.videoDimension.height) * this.screenDimension.height - this.screenDimension.height / 2);
    return [x, y];
  }

}