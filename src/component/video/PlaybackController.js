import { Grid, IconButton, Slider, withStyles, Button } from "@material-ui/core"
import { PlayArrow, SkipNext, SkipPrevious, Pause, Forward5, Replay5, Speed} from "@material-ui/icons"
import PlaybackDialogController from "./playbackDialogController"

const useStyles = theme => ({
    root: {
        
    },
    speedButton: {
        textTransform: 'none',
        color: '#5C5C5C'
    },
    speedButtonDisable: {
        textTransform: 'none',
        color: '#ACACAC'
    }
});

class PlaybackController extends React.Component {
    constructor(props) {
        super(props);
        
        this.state = {
            rate: 1,
            dialogOpen: false,
            playing: false,
            currentTime: 0
        }

        this.video = this.props.vid;
        this.frame = this.props.vidFrame;
        this.event = this.props.marks;
        this.event.push({
            value: this.video.duration,
            label: '-' + this.formatSeconds(this.video.duration - this.video.currentTime)
        });
        this.event.unshift({
            value: 0,
            label: this.formatSeconds(this.video.currentTime)
        });

        this.play = this.play.bind(this);
        this.seek = this.seek.bind(this);
        this.seekForward = this.seekForward.bind(this);
        this.seekBackward = this.seekBackward.bind(this);
        this.seekForwardFrame = this.seekForwardFrame.bind(this);
        this.seekBackwardFrame = this.seekBackwardFrame.bind(this);
        this.changePlayRate = this.changePlayRate.bind(this);
        this.openDialog = this.openDialog.bind(this);
        this.closeDialog = this.closeDialog.bind(this);
    }

    componentDidMount() {
        this.video.addEventListener('timeupdate', (event) => {
            this.setState({
                currentTime: this.video.currentTime
            });

            this.event.pop();
            this.event.push({
                value: this.video.duration,
                label: '-' + this.formatSeconds(this.video.duration - this.video.currentTime)
            });
            this.event.shift();
            this.event.unshift({
                value: 0,
                label: this.formatSeconds(this.video.currentTime)
            });
        })
    }

    formatSeconds(duration) {
        let minutes = Math.floor(duration / 60);
        let seconds = duration - (minutes * 60);


        if (minutes < 10) {
            minutes = "0" + minutes;
        }

        if (seconds < 10) {
            seconds = "0" + seconds;
        }

        return minutes + ':' + seconds.toString().slice(0, 2);
    }

    seek(time) {
        this.video.currentTime = time;
        this.props.updateCallback(this.frame.get());
    }

    seekForward(time) {
        this.video.currentTime += time;
    }

    seekBackward(time) {
        this.video.currentTime -= time;
    }

    seekForwardFrame(time) {
        return () => {
            this.frame.seekForward(time);
            this.props.updateCallback(this.frame.get());
        }
    }

    seekBackwardFrame(time) {
        return () => {
            this.frame.seekBackward(time);
            this.props.updateCallback(this.frame.get());
        }
    }

    changePlayRate(rate) {
        this.setState({
            rate: rate
        });
        this.video.playbackRate = rate;
    }

    play() {
        if (this.video.paused) {
            this.video.play();
            this.setState({
                playing: true
            })
        }
        else{
            this.video.pause();
            this.setState({
                playing: false
            })
        }
    }

    openDialog() {
        this.setState({
            dialogOpen: true
        });
    }

    closeDialog(rate) {
        this.setState({
            dialogOpen: false
        });
        this.changePlayRate(rate);
    }

    holdHandle(ref, callback) {
        let interval = setInterval(() => {
            callback();
        }, 100);

        ref.onmouseup = () => {
            clearInterval(interval);
        };
    }

    render() {
        const { classes } = this.props;
        const marks = this.event;
        return (
            <div 
                className={ classes.root }
                ref={ (ref) => { this.controlRef = ref } }>
                <Grid 
                    container
                    justify="center"
                    alignItems="center"
                    spacing={1}>
                    <Grid item xs={1}>
                        <IconButton onClick={ () => { this.seekBackward(5) } }>
                            <Replay5 />
                        </IconButton>
                    </Grid>
                    <Grid item xs={1}>
                        <IconButton onClick={ () => { this.play() } }>
                        {
                            this.state.playing ? <Pause /> : <PlayArrow />
                        }
                        </IconButton>
                    </Grid>
                    <Grid item xs={1}>
                        <IconButton onClick={ () => { this.seekForward(5) } }>
                            <Forward5 />
                        </IconButton>
                    </Grid>
                    {/* <Grid item xs={1}>
                        <IconButton onClick={ () => { this.openDialog() } }>
                            <Speed />
                        </IconButton>
                    </Grid> */}
                    <Grid item xs={1} />
                    <Grid item xs={1}>
                        <IconButton 
                            ref={ (bfButtonRef) => { this.bfButtonRef = bfButtonRef }} 
                            onMouseDown={ () => { this.holdHandle(this.bfButtonRef, this.seekBackwardFrame(1)) } }>
                            <SkipPrevious />
                        </IconButton>
                    </Grid>
                    <Grid item xs={1}>
                        <IconButton
                            ref={ (ffButtonRef) => { this.ffButtonRef = ffButtonRef }} 
                            onMouseDown={ () => { this.holdHandle(this.ffButtonRef, this.seekForwardFrame(1)) } }>
                            <SkipNext />
                        </IconButton>
                    </Grid>
                    <Grid item xs={1} />
                    <Grid item xs={1}>
                    {
                        this.state.rate == 0.1 &&
                        <Button className={ classes.speedButton } onClick={ () => { this.changePlayRate(1.0) } }><Speed />&nbsp;0.1x</Button>
                    }
                    {
                        this.state.rate == 0.5 &&
                        <Button className={ classes.speedButton } onClick={ () => { this.changePlayRate(0.1) } }><Speed />&nbsp;0.5x</Button>
                    }
                    {
                        this.state.rate == 1.0 &&
                        <Button className={ classes.speedButton } onClick={ () => { this.changePlayRate(0.5) } }><Speed />&nbsp;1.0x</Button>
                    }
                    </Grid>
                </Grid>
                <Grid 
                    container
                    justify="center"
                    alignItems="flex-start"
                    spacing={1}>
                    <Grid item xs={11}>
                        <Slider 
                            min={0}
                            max={this.video.duration}
                            defaultValue={0}
                            value={this.state.currentTime}
                            onChange={ (event, newValue) => { this.seek(newValue) } }
                            marks={ marks }
                            valueLabelDisplay="off"
                            getAriaValueText={(value) => { return `${value}s` }}
                        />
                    </Grid>
                </Grid>
                {
                    this.state.dialogOpen &&
                    <PlaybackDialogController selectedValue={this.state.rate} open={ this.state.dialogOpen } onClose={ (rate) => { this.closeDialog(rate) } }/>
                }
                {/* <Select
                    ref={ (rateRoot) => { this.rateRoot = rateRoot } } 
                    native
                    value={ this.state.rate }
                    onChange={ (event) => { this.changePlayRate(event.target.value) } }>
                    <option value={0.1}>0.1x</option>
                    <option value={0.5}>0.5x</option>
                    <option value={1}>1.0x</option>
                    <option value={2}>2.0x</option>
                </Select> */}
                   
            </div>
        );
    }
}

export default withStyles(useStyles)(PlaybackController);