import Paper from '@material-ui/core/Paper';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import MUIDataTable from "mui-datatables";
import EditBoard from "./event/EditBoard"
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import VideoController from'./videoController'
const useStyles = (theme) => ({
    root:{
        height:"100%",
        width:"100%"
    },
    labelName:{
        marginRight: theme.spacing(2),
        marginLeft: theme.spacing(2),
        display: "inline-block"
      },
    Button:{
      marginRight: theme.spacing(3),
      marginTop: theme.spacing(1),
    },
    videoLayout:{
        height:'50%'
    },
    editorLayout:{
        height:'50%'
    },
    tableLayout:{
        height:'50%'

    }
  });

//about table
  const inputJson=`{
    "eventlabels": [{
            "name": "拍面",
            "labels": [{
                "actions": ["正拍", "反拍"],
                "actionType": ""
            }]
        },
        {
            "name": "動作",
            "labels": [{
                "actions": ["發"],
                "actionType": "發球"
            }, {
                "actions": ["擺", "切", "劈"],
                "actionType": "控制"
            }, {
                "actions": ["擰", "拉"],
                "actionType": "進攻"
            }, {
                "actions": ["擋"],
                "actionType": "防禦"
            }]
        }, {
            "name": "落點",
            "labels": [{
                "actions": ["反手短", "中路偏反手短", "中路偏正手短", "正手短"],
                "actionType": "短"
            }, {
                "actions": ["反手長", "中路長", "正手長"],
                "actionType": "長"
            }]
        }
    ]
  }`

const columns = [
 {
  name: "time",
  label: "Time",
  options: {
   filter: true,
   sort: true,
  }
 },
 {
  name: "type",
  label: "Type",
  options: {
   filter: true,
   sort: false,
  }
 },
 {
  name: "info",
  label: "Info",
  options: {
   filter: true,
   sort: false,
  }
 },
];

const data = [
 { time: "1:12", type: "得分", info: "0:1"},
 { time: "5:13", type: "動作", info: "正手"},
 { time: "9:22", type: "動作", info: "反手"},
];


const buttonJSON=JSON.parse(inputJson);
class EventController extends React.Component {
    constructor(props) {
        super(props);
        this.state={
            time:"10:55",
            action:"",
            face:"",
            placement:"",
            type:"動作",
            buttonInfos:buttonJSON,
            tableList:data,
            value:0,
            p1Score:'',
            p2Score:'',
        }
        this.handleEditor=this.handleEditor.bind(this);
        this.newButton=this.newButton.bind(this);
        this.rowDelete=this.rowDelete.bind(this);
    }
    handleEditor(event){
        const {value}=event.currentTarget;
        let buttonTypes=['face','action','placement'];
        let name='';
        for(let i in this.state.buttonInfos.eventlabels){
            for(let j in this.state.buttonInfos.eventlabels[i].labels){
                for(let k in this.state.buttonInfos.eventlabels[i].labels[j].actions){
                    if(value===this.state.buttonInfos.eventlabels[i].labels[j].actions[k]){
                        name=buttonTypes[i];
                    }
                }
            }
        }
        this.setState({[name]:value})
    }
    handleTabChange = (event, value) => {
        this.setState({ value:value });
        console.log(this.state.value)
        if(value===0)
            this.setState({type:'動作'});
        else
            this.setState({type:'得分'});
      }
    handleForm=(e)=>{
        const {id,value}=e.target
        this.setState({[id]:value})
        console.log(id)
        console.log(value)
    }
    rowDelete(rowsDeleted,data){
        this.setState(state => {
            const list = state.tableList.filter((item, j) =>{
                for(data of rowsDeleted.data)
                {
                    // console.log(data.index)
                    if(data.index === j){
                        return false
                    }
                }
                return true
            });
            return {
              tableList:list,
            };
          });
    }
    newButton(e){
            
        const newCol= this.state.value===0?{time:this.state.time,type:this.state.type,info:this.state.face+this.state.action+this.state.placement}
            :{time:this.state.time,type:this.state.type,info:this.state.p1Score+':'+this.state.p2Score};
        this.setState(state=>{
            const tableCol=this.state.tableList.concat(newCol);
            return{
                tableList:tableCol,
                action:"",
                face:"",
                placement:"",
                p1Score:"",
                p2Score:"",
            };
        });
    }
    getMuiTheme = () => createMuiTheme({
        overrides: {
          MUIDataTableBodyCell: {
            root: {
              backgroundColor: "#FFF",
            }
          }
        }
      })
    render() {
        const options = {
            filterType: 'checkbox',
            tableBodyHeight: '250px',
            pagination:false,
            print:false,
            onRowsDelete:this.rowDelete,
          };
        const { classes } = this.props;
        return (
        <div>
            
            <Grid container spacing={2} className={classes.root}>
                <Grid item xs={8} className={classes.videoLayout}>
                    <VideoController/>
                </Grid>
                <Grid item xs={4} className={classes.tableLayout}>
                    <EditBoard handleEditor={this.handleEditor} 
                                eventInfos={this.state} 
                                handleTabChange={this.handleTabChange} 
                                handleForm={this.handleForm}/>
                    <Typography className={classes.labelName}>control:</Typography>
                    <Button variant="contained" className={classes.Button} onClick={this.newButton}>新增</Button>
                    <Button variant="contained" className={classes.Button}>上一個</Button>
                    <Button variant="contained" className={classes.Button}>下一個</Button>
                    <Button variant="contained" className={classes.Button}>匯出</Button>
                </Grid>
                <Grid item xs={12} className={classes.tableLayout}>
                    <MuiThemeProvider theme={this.getMuiTheme()}>
                        <MUIDataTable
                            title={"Event List"}
                            data={this.state.tableList}
                            columns={columns}
                            options={options}
                        />
                    </MuiThemeProvider>
                </Grid>
            
            </Grid>
        </div>
        );
    }
}

export default withStyles(useStyles)(EventController);