const functions = require('firebase-functions');
const fs = require('fs')
const cors = require('cors')({
    origin: true,
});
const admin = require('firebase-admin');
// var serviceAccount = require("../../sources/firebaseadmin/couthathena-tabletennis-firebase-adminsdk-ndxz6-ccbbf04d11.json");

admin.initializeApp({
    credential: admin.credential.cert({
        "type": "service_account",
        "project_id": "couthathena-tabletennis",
        "private_key_id": "ccbbf04d1104999d1704f3afaff4dd9b037a71f9",
        "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQC15Zcy1CxZKY3L\n3nftK2XS8a/cMtXNqWzf1370dFMFutxp/VEsqDFYF5MhxA8PCL/PLovFUw6BbmCI\npET0jlfvTeRZM/nfTOqx71GONFn1bQgH+Bxf+MbHWqB4R6YCNhD3ZcCcitxxllUo\nuID2ZedO47e/4ilqBdX1EaOnnBumWfjcBcz1qpLB/8btQFCerP0MhPLjnOtqIloK\nrCFaQAneOQVPKhX7GjL2MjldAg7FGLqykXBccFrOfPXo+DUy1cPkgxOHnNb3CEUh\nugixMMFv1yxcmnxN/yHIu4XhYJN8NYp5ze5IxbcJs8xOzRDv+jpuXhCMB78O3YBy\n1/yPfgRJAgMBAAECggEAK0id6Rd6EiGEP24J5aL9EOw4J9Lk85KMeNuDt1Hy65si\nlgoUhebkDw1VEqTf3Hj9g6SU1/jCq+QDDqDgU72HiUdkviFTJWLdOkgzhP5KJc+z\nKIpO3juJuaiEJPsPstkAwo4w/k9A07qtOwpkjb2EBE7Fy+BacINA/Zcb9Wd9YOEC\nexM1f8aIKBBMQyvFmtg0Civ6QhsZWcTSpZxji48fXsGdv0rwAIp1Sni8z0jfwWAw\nzQqmM5RUZkn/saftU5NXqH27o5FDgAJxnQVK9o0CDYli30ouh9ZBBQ4N6Gqn2lU+\ngQOpOsdmgP04vslDgovvyuIfBYO/npAPSmXpC6kjsQKBgQD1j9UiIPmq4Hx6g+jt\nF+y30QCGhqlfNnDkJiwDZyaHl/DYUKNO9haYa45sUrdR1dgTlX8SO/HxLTaL5pgG\nDWQm8bII5wgrxAAaF67euybHCI0zuawi01v/wXkxQymJrE0hFrHBEv07B8SCrL0j\n8k/u8GnOGIBUATMdrjCBLcK6XQKBgQC9oPcF41EW6wKQCSVXbzbf3EpjK0WqwN0i\nqKYdyUt2ZEX3wp1xQc9B4euyP+032ZGLqbqbOMN9TVHOhvQjNDTv/1GbD/6Un58Z\nPLhIQBLMvjOwTG/SWKC44cPM4wUEDOw5BdWaL3xUA1FwvBSmB5sOQ9xtAZvrQOMi\n/INPsNGK3QKBgGpPs/FLn2uINrv/4dbLtdM66+RR5n/vHN7ZW9K5VKf9curjtmAY\nEF46qnVhNjZ6w0pvPY1SYIOOlH/q/EQahhKKLw7Fnvb7qyOgvzd0hOxEWoVbTBfj\ns3+9bk+SiSgubVH29kvlgbaNvVjpmhU28b47XUM5vk0gG9YstJkoi5aNAoGABBvU\nvl/qheCZsZ1BHd/fBIK1aeiqHcUIBZQ55TNTW0FY8sFYv/d5Eihe/wG7lhY0vR30\nWSClu7VNLHhoGcsryndxwTZbgcTjoZxbMdWRkrYcUJmXYqtg8CCUKYBRv4C4LZQe\nyLR2sStRPGwW1nIbvfdic0oxoqtJQJzQC439PFECgYEAvse662DmWRvtNKjMQHoL\nm1DmDleK0CThRg5ycr/9YJYYGW/MsOxzI4hYKueJYyowuIRNVBclpVNFWWPgdzfN\nxKTFUam8F5f3uAjddDLW8kUPCGl0rTbpLpWsNPbLMpROxScJv1u66DqvcsrfZg+E\nwA1hdKQERq9iPptXzBc9zTg=\n-----END PRIVATE KEY-----\n",
        "client_email": "firebase-adminsdk-ndxz6@couthathena-tabletennis.iam.gserviceaccount.com",
        "client_id": "107501416415794250036",
        "auth_uri": "https://accounts.google.com/o/oauth2/auth",
        "token_uri": "https://oauth2.googleapis.com/token",
        "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
        "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/firebase-adminsdk-ndxz6%40couthathena-tabletennis.iam.gserviceaccount.com"
    }),
    databaseURL: "https://couthathena-tabletennis.firebaseio.com"
});

const storageApp = admin.storage().bucket("couthathena-tabletennis.appspot.com");

const matchpath = 'matches'
const playerpath = 'players'
const idPath = 'ids'
const eventpath = 'events'
const eventlabelpath = 'eventlabels'


function makeUniqueID() {
    return new Promise(async(res, rej) => {
        const length = 10;
        var result = '';
        var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        var charactersLength = characters.length;

        for (var i = 0; i < length; i++) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        console.log("random string result")
        console.log(result)
        let IDref = admin.database().ref(`/${idPath}`).child(`${result}`)

        let IDsnapshot = await IDref.once("value")
        let isExist = IDsnapshot.exists();

        if (isExist == false) {
            res(result);
        } else {
            res(makeUniqueID());
        }
    })
}

/**
 * Getter
 */
exports.getMatchList = functions.https.onCall(async(data, context) => {
    console.log(data)
    const group = data.group;
    const type = data.type;

    try {
        let matchListResult = await admin.database().ref(`/groups/${group}/${type}/${matchpath}`).once('value')
        if (matchListResult) {
            return {
                response: matchListResult.val()
            }
        } else {
            throw "Match List is empty"
        }

    } catch (e) {
        return {
            response: "Match List Process Error",
            error: e
        }
    }
})

exports.getMatchNode = functions.https.onCall((data, context) => { // TODO : check if necessary

})

exports.getPlayerList = functions.https.onCall(async(data, context) => { // TODO

    const group = data.group;
    const type = data.type;
    try {
        let playerListResult = await admin.database().ref(`/groups/${group}/${type}/${playerpath}`).once('value')
        if (playerListResult) {
            return {
                response: playerListResult.val()
            }
        } else {
            throw Error("Player List is empty")
        }

    } catch (e) {
        return {
            response: "Player List Process Error",
            error: e
        }
    }
})

exports.getEventList = functions.https.onCall(async(data, context) => {
    const group = data.group;
    const type = data.type;
    try {
        let eventListResult = await admin.database().ref(`/groups/${group}/${type}/${eventpath}`).once('value')
        if (eventListResult) {
            return {
                response: eventListResult.val()
            }
        } else {
            throw Error("Player List is empty")
        }

    } catch (e) {
        return {
            response: "Player List Process Error",
            error: e
        }
    }
})

exports.getEventLabels = functions.https.onCall(async(data, context) => {

    const group = data.group;
    const type = data.type;
    try {
        let eventlabelResult = await admin.database().ref(`/groups/${group}/${type}/${eventlabelpath}`).once('value')
        if (eventlabelResult) {
            return {
                response: eventlabelResult.val()
            }
        } else {
            throw Error("Event Label is empty")
        }

    } catch (e) {
        return {
            response: "Event Label Process Error",
            error: e
        }
    }
})

/**
 * Setter
 */
exports.addMatchNode = functions.https.onCall(async(data, context) => {
    const group = data.group;
    const type = data.type;
    const team1 = data.team1
    const team2 = data.team2
    const matchname = data.matchname
    const score = data.score

    let date = new Date()
    let matchdata = {
        createTime: date.toUTCString(),
        info: {
            team1: team1,
            team2: team2
        },
        name: matchname,
        set: score
    }

    try {
        let matchid = await makeUniqueID()
        let addMatchResult = await admin.database().ref(`/groups/${group}/${type}/${matchpath}/${matchid}`).set(matchdata)
        let addIdResult = await admin.database().ref(`${idPath}`).push(matchid)

        if(addMatchResult){
            return {
                message: "Add new match node successfully!",
                matchid: matchid
            };
        }else {
            throw Error("process failed")
        }
    } catch (e) {
        return {
            message: "Add new match node failed! ",
            error: e
        };
    }
});

exports.saveMatchNode = functions.https.onCall(async(data, context) => { // TODO
    const group = data.group;
    const type = data.type;
    const matchid = data.matchid;
    const team1 = data.team1
    const team2 = data.team2
    const matchname = data.matchname
    const score = data.score

    try {
       
        let addMatchResult = await admin.database().ref(`/groups/${group}/${type}/${matchpath}/${matchid}`).set(matchdata)
        let addIdResult = await admin.database().ref(`${idPath}`).push(matchid)

        if(addMatchResult){
            return {
                message: "Add new match node successfully!",
                matchid: matchid
            };
        }else {
            throw Error("process failed")
        }
    } catch (e) {
        return {
            message: "Add new match node failed! ",
            error: e
        };
    }

})

exports.addPlayerNode = functions.https.onCall(async(data, context) => {
    const group = data.group;
    const type = data.type;
    const playerdata = data.playerdata;

    let playerInfo = {
        name: playerdata.name
    }

    try {
        let playerid = await makeUniqueID()
        let addPlayerResult = await admin.database().ref(`/groups/${group}/${type}/${playerpath}/${playerid}`).set(playerInfo)
        let addIdResult = await admin.database().ref(`${idPath}`).push(playerid)
        if (addPlayerResult) {
            return {
                message: "Add new player node successfully!",
                plaierid: playerid
            };
        } else {
            throw Error("process failed")
        }
    } catch (e) {
        return {
            message: "Add new player node failed!",
            error: e
        };
    }
})

exports.savePlayerNode = functions.https.onCall(async(data, context) => { // TODO

})

exports.setEventList = functions.https.onCall(async(data, context) => {
    const group = data.group;
    const type = data.type;
    const matchid = data.id;
    const eventdata = data.eventdata;

    try {
        let setEventResult = await admin.database().ref(`/groups/${group}/${type}/${matchpath}/${matchid}/${eventpath}`).set(eventdata)
        
        if (setEventResult) {
            return {
                message: "Set event successfully!",
                plaierid: playerid
            };
        } else {
            throw Error("process failed")
        }
    } catch (e) {
        return {
            message: "Set event failed!",
            error: e
        };
    }
})

exports.setEventLabels = functions.https.onCall(async(data, context) => {
    const group = data.group;
    const type = data.type;
    const eventlabels = data.eventlabels
    try {
        let setEventResult = await admin.database().ref(`/groups/${group}/${type}/${eventlabelpath}`).set(eventlabels)
        if (setEventResult) {
            return {
                message: "Set event labels successfully!"
            };
        } else {
            throw Error("process failed")
        }
    } catch (e) {
        return {
            message: "Set event labels failed!",
            error: e
        };
    }
})

exports.makeFilePublic = functions.https.onCall(async(data, context) => {
    const path = data.path
    const file = storageApp.file(`${path}`)
        // https://storage.googleapis.com/couthathena-tabletennis.appspot.com/testingGroup/videos/FrEswUsWOW.mp4

    try {
        let status = await file.isPublic();
        if (status[0] == false) {
            let makePublicResult = await file.makePublic()
            if (makePublicResult) {
                return {
                    message: "Public file process successfully"
                }
            } else {
                throw Error("Public file process failed")
            }

        } else {
            throw Error("File is already public")
        }
    } catch (e) {
        return {
            message: "Public file failed",
            error: e
        }
    }
})